package pdt;

public class KlasGroep {
    private String naam;

    public KlasGroep(String naam) {
        this.naam = naam;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("KlasGroep{");
        sb.append("naam='").append(naam).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
